package com.synechron.coecore.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet{
	

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String error=req.getParameter("error");
		if(error!=null){
			req.setAttribute("error", true);
			req.setAttribute("msg", "Incorrect username or password. Please retry.");
		}

		RequestDispatcher requestDispatcher=req.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		requestDispatcher.forward(req, res);
	}
}
