package com.synechron.coecore.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends HttpServlet{
	
	/*@Autowired
	private ContactService contactService;*/
	
	

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		
		req.setAttribute("title", "Spring Security Hello World");
		req.setAttribute("message", "This is protected page!");

		RequestDispatcher requestDispatcher=req.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
		requestDispatcher.forward(req, res);
	}
}
