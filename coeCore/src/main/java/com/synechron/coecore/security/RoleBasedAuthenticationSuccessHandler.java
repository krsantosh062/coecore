package com.synechron.coecore.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class RoleBasedAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private RedirectStrategy redirectStrategy; 
			
	public RoleBasedAuthenticationSuccessHandler(){
		redirectStrategy = new DefaultRedirectStrategy();
	}	
	
	public void onAuthenticationSuccess(HttpServletRequest request,	HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		String nextPage = determineRedirectPage(authentication);
		getRedirectStrategy().sendRedirect(request, response, nextPage);
	}

	public String determineRedirectPage(Authentication authentication){
		Collection<? extends GrantedAuthority> authorities =  authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities){			
			if("ROLE_ADMIN".equalsIgnoreCase(grantedAuthority.getAuthority())) {
				return "/admin/home.html";
			}
		}
		return "/home.html"; // Default Page
	}
	public RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}
}