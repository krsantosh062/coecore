<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<script type="text/javascript">
	$('ul.menuNav li a.overview').addClass('active');
	$('ul.menuNav li a.home').css('background-color','white');
	$('ul.menuNav li a.automationTab').addClass('active');
</script>
<header>
    <nav class="container">
      <ul class="menuNav">
        <li><a class="overview" href="automationOverview.html">Overview</a></li>
        <li><a class="controller" href="<c:url value="/controller.html"/>">Controller</a></li>
        <li><a class="tools" href="<c:url value="/tools.html"/>">Tools</a></li>
        <li><a class="reports" href="<c:url value="/reports.html"/>">Reports</a></li>
        <li><a class="defectmanagement" href="<c:url value="/defectManagement.html"/>">Defect Management</a></li>
      </ul>
    </nav>
</header>