<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script src="js/html5shiv.js"></script>
<!-- Date picker related jQuery -->
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
</head>
<body>
	<div id="main">
		<!-- main div start -->
		<tiles:insertAttribute name="header" />
		<tiles:insertAttribute name="subHeader" />
		<div class="clear"></div>
		<div id="main-content">
			<!-- main content start here -->
			
			<tiles:insertAttribute name="body" />
		</div>
		<!-- end of main content -->
	</div>
	<!-- End of main -->
</body>
</html>