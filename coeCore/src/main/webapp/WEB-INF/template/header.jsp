<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<header>
    <div class="top_bar">
      <div class="container">
        <img src="images/Synechron_logo.png" class="logo1" /><div class="logo">Syne<span class="whitetxt">Center</span></div>
      </div>
    </div>
    <nav class="container">
      <ul class="menuNav">
        <li><a class="home" href="<c:url value="/home.html"/>">Home</a></li>
        <%--  <li><a class="manualTab">Manual</a></li> --%>
        <%--  <li><a class="automationTab" href="<c:url value="/automationOverview.html"/>">Automation</a></li> --%>
        <li><a class="performanceTab" href="<c:url value="/performanceOverview.html"/>">Performance</a></li>
        <%-- <li><a class="mobileTab">Mobile</a></li> --%>
        <li><a class="logoutTab" href="${pageContext.request.contextPath}">Log Out</a></li>
      </ul>
    </nav>
</header>
<script type="text/javascript">
	$('ul.menuNav li a.performanceTab').css('background-color','white');
	$('ul.menuNav li a.home').addClass('active');
	$('ul.menuNav li a.home').css('background-color','#959595');
</script>