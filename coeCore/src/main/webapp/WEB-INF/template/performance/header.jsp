<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<script type="text/javascript">
	$('ul.menuNav li a.overview').addClass('active');
	$('ul.menuNav li a.home').css('background-color','white');
	$('ul.menuNav li a.performanceTab').addClass('active');
</script>
<header>
    <nav class="container">
      <ul class="menuNav">
        <li><a class="overview" href="performanceOverview.html">Overview</a></li>
        <li><a class="createscenario" href="<c:url value="/showScenario.html"/>">Edit Scenario</a></li>
        <li><a class="scheduletest" href="<c:url value="/testScenario.html"/>">Schedule Test</a></li>
        <li><a class="livereport" href="<c:url value="/liveReport.html"/>">Live Report</a></li>
        <li><a class="historicreport" href="<c:url value="/historicalReports.html"/>">Historical Reports</a></li>
        <li><a class="configure" href="<c:url value="/configure.html"/>">Configure</a></li>
        <%-- <li><a id="toolInfoLink" class="toolinfo" href="<c:url value="/toolInfo.html"/>">Tool Info</a></li> --%>
      </ul>
    </nav>
</header>