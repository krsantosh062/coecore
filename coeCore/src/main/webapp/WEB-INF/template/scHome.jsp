<script type="text/javascript">
	$('#toolInfoLink').addClass('active');
</script> 
<div id="bodyContentDiv">
    <section class="wrap-content">
      <div class="col">
        <div class="tool_disc"> Tool description </div>
        <div class="col_content"><span class="tooltip"></span>SyneCenter tool provides an integrated platform built on Jmeter with rich features and user interface to provide an end to end performance testing and monitoring solution. SyneCenter can be used for Performance Test execution, Monitoring and Reporting.</div>
      </div>
      <!-- end of col1 -->
      <div class="col">
        <div class="tool_disc">Create Scenario</div>
        <div class="col_content"> <span class="tooltip"></span>One of the most challenging aspects of the Performance testing is to design a realistic test scenario. This section enables the user to define a desired workload model using parameters like think time, Pacing, Duration, Ramp Up, Ramp Down. </div>
      </div>
      <!-- end of col2 -->
      <div class="col mright">
        <div class="tool_disc"> Schedule Scenario</div>
        <div class="col_content"><span class="tooltip"></span>This sections enables user to schedule the Performance test execution and add/remove Load generator and select Test environment servers to be monitored during the execution. </div>
      </div>
      </section>
    <section class="wrap-content mtop">
      <div class="col half first">
        <div class="tool_disc"> Live Reports</div>
        <div class="col5_content">
         <p> <img src="images/response_time_chart.png" alt="" border="0"/> </p>
         <p>This section enables user to monitor the execution with online graphs and data  which includes all client side metrics such as transaction response time, Hits/sec,  transaction pass/fail summary etc. as well as server side metric such as CPU utilization, Available Memory etc. for ongoing load test. </p>
        </div>
      </div>
      <div class="col half">
        <div class="tool_disc"> Historic Reports</div>
        <div class="col5_content">
         <p> <img src="images/response_time_chart.png" alt="" border="0"/> </p>
         <p>This section enables user to view any past test run reports with  all client side as well as server side metrics for analysis and comparison purpose. </p>
        </div>
      </div>
    </section>
</div>
 