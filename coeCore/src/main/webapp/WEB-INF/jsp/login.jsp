
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>coeCore::Login</title>
<link rel="stylesheet" type="text/css"
	href="http://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script src="js/html5shiv.js"></script>
<!-- Date picker related jQuery -->
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
</head>
<body>
	<div id="main">
		<!-- main div start -->
		

<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<!DOCTYPE html>
<header>
    <div class="top_bar">
      <div class="container">
        <img src="images/Synechron_logo.png" class="logo1" /><div class="logo">Syne<span class="whitetxt">Center</span></div>
      </div>
    </div>
</header>

		
		<div class="clear"></div>
		<div id="main-content">
			<!-- main content start here -->
			
			  
<html>
<head>
<title>coeCore</title>
</head>
<body>
	<center>
		<h2>Performance Center Login</h2>
		<br/>
		<%
			String errorString = (String)request.getParameter("error");
			if(errorString != null && errorString.trim().equals("true")){
				out.println("<h5><font color='red'>Incorrect username or password. Please retry.</font></h5><br/>");
			}
		%>
		
		<div style="text-align: center; padding: 10px; border: 1px solid green;">
			<form method="post" action="j_spring_security_check">
				<table style="width:352px;margin:auto;">
					<tr>
						<td colspan="2" style="color: red"></td>
					</tr>
					<tr>
						<td>Username:</td>
						<td><input type="text" name="j_username" class="loginuserpass"/></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="j_password" class="loginuserpass"/></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Submit" /></td>
					</tr>
					<input type="hidden" name="" value="" />
				</table>
			</form>
		</div>
	</center>
</body>
</html>

<script type="text/javascript">
	$('#main-content').css({'width':'372px','margin-top':'150px'});
</script>
		</div>
		<!-- end of main content -->
	</div>
	<!-- End of main -->
</body>
</html>