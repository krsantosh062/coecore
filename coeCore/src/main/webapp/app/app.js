var synecenter	=	angular.module("synecenter",
									['ui.router',
									 "common.services",
									 "angularFileUpload"
									 ]);




synecenter.config(function($stateProvider,$urlRouterProvider){
	
	$urlRouterProvider.otherwise('/');
	
	$stateProvider
	
	.state('home',{
		
		url:'/',
		templateUrl:'app/home.html'
	})
	
	
	.state('manual',{
		
		url:'/manual',
		templateUrl:'app/manual/manual.html'
	})
	
	
	
	.state('automation',{
		url:'/automation',
		templateUrl:'app/automation/automation.html',
		controller: "automationController as ac"
	})
	
	.state('automation.automationOverView',{
		url: '/automationOverView',
		templateUrl: 'app/automation/automationOverView.html'
	})
	
	.state('automation.automationOverViewGridDetails',{
		url: '/automationOverViewGridDetails',
		templateUrl: 'app/automation/automationOverView.html'
	})
	
	.state('automation.automationController',{
		url: '/automationController',
		templateUrl: 'app/automation/automationController.html'
	})
	.state('automation.automationTools',{
		url: '/automationTools',
		templateUrl: 'app/automation/automationTools.html'
	})
	.state('automation.automationReports',{
		url: '/automationReports',
		templateUrl: 'app/automation/automationReports.html'
	})
	.state('automation.automationDefectManagement',{
		url: '/automationDefectManagement',
		templateUrl: 'app/automation/automationDefectManagement.html'
	})
	
	
	
	.state('performance',{
		url:'/performance',
		templateUrl:'app/performance/performance.html',
		controller: "performanceController as vm"
		
	})
	.state('performance.performanceOverView',{
		url:'/performanceOverView',
		templateUrl:'app/performance/performanceOverView.html'
	})
	.state('performance.performanceEditScenario',{
		url:'/performanceEditScenario',
		templateUrl:'app/performance/performanceEditScenario.html'
	})
	.state('performance.performanceScheduleTest',{
		url:'/performanceScheduleTest',
		templateUrl:'app/performance/performanceScheduleTest.html'
	})
	.state('performance.performanceLiveReport',{
		url:'/performanceLiveReport',
		templateUrl:'app/performance/performanceLiveReport.html'
	})
	.state('performance.performanceHistoricalReport',{
		url:'/performanceHistoricalReport',
		templateUrl:'app/performance/performanceHistoricalReport.html'
	})
	.state('performance.performanceConfigure',{
		url:'/performanceConfigure',
		templateUrl:'app/performance/performanceConfigure.html'
	})
	
	.state('performance.performanceConfigure.performanceConfigureLoadGenerators',{
		url:'/ConfigureLoadGenerators',
		templateUrl:'app/performance/ConfigureLoadGenerators.html'
	})
	.state('performance.performanceConfigure.performanceConfigureServers',{
		url:'/ConfigureServers',
		templateUrl:'app/performance/ConfigureServers.html'
	})
	
	
	
	
	.state('performance.performanceToolInfo',{
		url:'/performanceToolInfo',
		templateUrl:'app/performance/performanceToolInfo.html'
	})
	
	
	.state('mobile',{
		url:'/mobile',
		templateUrl:'app/mobile/mobile.html'
	});
	
	
});