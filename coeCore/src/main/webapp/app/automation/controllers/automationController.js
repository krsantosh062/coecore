(function() {

	angular.module("synecenter").controller(
			"automationController",
			[ "automationOverviewServices", "deleteProjectService", "$scope",
					"$window", "$location", "$resource", "$http",
					automationController ]);

	function automationController(automationOverviewServices,
			deleteProjectService, $scope, $window, $location, $resource, $http) {

		//read data from databse   

		var ac = this;
		automationOverviewServices.query(function(data) {
			ac.recentRest = data;
		});

		//new project Popup	

		$scope.showModal = false;
		$scope.toggleModal = function(projectId) {
			$scope.showModal = !$scope.showModal;

		};
		
		$scope.toggleModal = function() {
			$scope.showModal = !$scope.showModal;

		};

		//delete data from database

		ac.ShowConfirm = function(projectId) {
			console.log(projectId);
			if ($window.confirm("Please confirm?")) {

				var id = projectId;
				deleteProjectService.deleteProject({
					projectId : id
				});

				$scope.Message = "You clicked YES.";
				
				var ac = this;
				automationOverviewServices.query(function(data) {
					ac.recentRest = data;
				});

			} else {
				$scope.Message = "You clicked NO.";
			}
		};

		// code for save

		ac.projectTypeNameOptions = [ {
			projectTypeId : "agile",
			value : 1
		}, {
			projectTypeId : "waterfall",
			value : 2
		}, {
			projectTypeId : "spiral",
			value : 3
		} ];

		// code for save

		var addProjectService = $resource('rest/autoconfig/saveProjectDetails');

		ac.addNewProject = function() {

			var newProject = {
				"projectDescription" : ac.newProject.projectDescription,
				"projectName" : ac.newProject.projectName,
				"projectTypeId" : ac.newProject.projectTypeId.value
			};

			addProjectService.save(newProject, function(data) {
				alert("project saved succefully" + data);
			});

			$scope.showModal = false;

			//  $scope.$apply();
		};

		
		
		//code for edit data

		ac.projectTypeIdOptions = [ {
			projectTypeId : "agile",
			value : 1
		}, {
			projectTypeId : "waterfall",
			value : 2
		}, {
			projectTypeId : "spiral",
			value : 3
		} ];
		
		$scope.showEdit = false;

		$scope.toggleModalEdit = function(projId) {

			var projectId = projId;

			$http.get('rest/autoconfig/getProjectDetailsById/' + projectId)
					.success(function(Data) {
						ac.updateProject = Data;
						
						
					});

			$scope.showEdit = !$scope.showEdit;
	
			$scope.updateProject = function() {

				var dd = {
					"projectName" : ac.updateProject.projectName,
					"projectDescription" : ac.updateProject.projectDescription,
					"projectTypeId" : ac.updateProject.projectTypeId.value
				};

				$http.put('rest/autoconfig/updateProjectDetails/' + projectId,
						dd);

				alert("Project data Updated successfully" + projectId);

				$scope.showEdit = false;

			};

		};

		//code for  set default 

		ac.setDefaultValues = function(projectId) {

			var ProjectId = projectId;

			$http.get(
					'rest/autoconfig/setDefaultProjectDetailsById/'
							+ ProjectId).success(function(Data) {
				$scope.defaultProjectDetails = Data;
			});

		};

	}
	;

	//popup for new project creation

	angular.module("synecenter")
			.directive(
					'modal',
					function() {
						return {
							template : '<div class="modal fade">'
									+ '<div class="modal-dialog">'
									+ '<div class="modal-content">'
									+ '<div class="modal-header">'
									+ '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
									+ '<h4 class="modal-title"></h4>'
									+ '</div>'
									+ '<div class="modal-body" ng-transclude></div>'
									+ '</div>' + '</div>' + '</div>',
							restrict : 'E',
							transclude : true,
							replace : true,
							scope : true,
							link : function postLink(scope, element, attrs) {
								scope.title = attrs.title;

								scope.$watch(attrs.visible, function(value) {
									if (value == true)
										$(element).modal('show');
									else
										$(element).modal('hide');
								});

								$(element).on('shown.bs.modal', function() {
									scope.$apply(function() {
										scope.$parent[attrs.visible] = true;
									});
								});

								$(element).on('hidden.bs.modal', function() {
									scope.$apply(function() {
										scope.$parent[attrs.visible] = false;
									});
								});
							}
						};
					});

	//popup for edit

	angular.module("synecenter")
			.directive(
					'editModal',
					function() {
						return {
							template : '<div class="editModal fade">'
									+ '<div class="editModal-dialog">'
									+ '<div class="editModal-content">'
									+ '<div class="editModal-header">'
									+ '<button type="button" class="close" data-dismiss="editModal" aria-hidden="true">&times;</button>'
									+ '<h4 class="editModal-title"></h4>'
									+ '</div>'
									+ '<div class="editModal-body" ng-transclude></div>'
									+ '</div>' + '</div>' + '</div>',
							restrict : 'E',
							transclude : true,
							replace : true,
							scope : true,
							link : function postLink(scope, element, attrs) {
								scope.title = attrs.title;

								scope.$watch(attrs.visible, function(value) {
									if (value == true)
										$(element).editModal('show');
									else
										$(element).editModal('hide');
								});

								$(element).on('shown.bs.editModal', function() {
									scope.$apply(function() {
										scope.$parent[attrs.visible] = true;
									});
								});

								$(element)
										.on(
												'hidden.bs.editModal',
												function() {
													scope
															.$apply(function() {
																scope.$parent[attrs.visible] = false;
															});
												});
							}
						};
					});

}());
