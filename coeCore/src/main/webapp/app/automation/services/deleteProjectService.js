(function(){
	"use strict";
	angular
		.module("common.services")
		.factory("deleteProjectService",
				["$resource",
				 deleteProjectService]);
	
	function deleteProjectService($resource){
		
		return $resource('rest/autoconfig/deleteProjectDetails/:projectId', {}, {
		
			deleteProject: { method: 'POST', params: {projectId: '@projectId'} }
			
	    });
	}
	
}());



	