(function(){
	"use strict";
	angular
		.module("common.services")
		.factory("automationOverviewServices",
				["$resource",
				 automationOverviewServices]);
	
	function automationOverviewServices($resource){
		return $resource("rest/autoconfig/automationOverview");
	}
	
	
}());

