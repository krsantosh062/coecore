(function(){
	"use strict";
	angular
		.module("common.services")
		.factory("editProjectService",
				["$resource",
				 editProjectService]);
	
	function editProjectService($resource){
		
		return $resource('rest/autoconfig/updateProjectDetails');
	}
	
}());



	