(function(){
	
	angular
		.module("synecenter")
		.controller("performanceController",
				["performanceHomeServices",
				 "performanceConfigureService",
				 "performanceConfigureService2",
				 "fileUpload",
				 "$resource",
				 "$http",
				 performanceController]);
	
	
	function performanceController (performanceHomeServices,performanceConfigureService,performanceConfigureService2,fileUpload,$resource,$http){
		var vm = this;
		
		performanceHomeServices.query(function (data){
			console.log(data)
			vm.recentRest = data;
		});
		
		
		performanceConfigureService.get(function(data){
			vm.lgList		=	data.loadGenerators;
			vm.serverLists	=	data.servers;
		});
		
		
		var saveService = $resource('rest/performance/saveLoadGenerator');
		vm.AddLg	=	function(){
			var newLG = {"loadGeneratorName" : vm.NewloadGeneratorName , "loadGeneratorIp": vm.NewloadGeneratorIp};
			saveService.save(newLG);
		}
		
		//var removeLGService = $resource('rest/performance/removeLoadGenerator');
		vm.removeLG  = function(lg){
			performanceConfigureService2.post({ id: lg.loadGeneratorId });
		}
		
		
		
		vm.servers = [
			              {server:"abc",ip:"123",type:"ws"},
			              {server:"pqr",ip:"345",type:"as"},
			              {server:"xyz",ip:"456",type:"ds"},
			              {server:"asd",ip:"654",type:"ws"},
			              {server:"qwe",ip:"456",type:"ws"},
			              {server:"fgh",ip:"777",type:"ds"},
			              {server:"fgh",ip:"888",type:"ds"},
			              {server:"fgh",ip:"999",type:"ds"}
		              ];
		
		
		//vm.newServer = {};
		
		vm.serverOptions = [
			                    {type: "Web Server Test" , value:"1"},
			                    {type: "Application Server test" , value:"2"},
			                    {type: "Database Server Test" , value:"3"}
		                    ];
		
		var saveServerService = $resource('rest/performance/saveServer');
		vm.AddServer = function(){
			alert(vm.newServer.server + " " + vm.newServer.ip + " " + vm.newServer.type.value);
			var newServer = {"serverName" : vm.newServer.server , "serverIp": vm.newServer.ip , "serverType" : vm.newServer.type.value};
			saveServerService.save(newServer);
			
		}
		
		
		
		
		
		vm.liveReportsID = [
		                    {id:"255" , value: 255},
		                    {id:"253" , value: 253},
		                    {id:"250" , value: 250}
		                   ];
		vm.historyReportsID = [
		                    {id:"255" , value: 255},
		                    {id:"253" , value: 253},
		                    {id:"250" , value: 250}
	                   ];
		
		
		vm.showLiveReportsDetails = function(){
			$('ul.menuNav li a.performanceTab').addClass('active');
			$('ul.menuNav li a.historicreport').addClass('active');
			
			$('#chart1').html("");
	    	if (vm.selHistoryReportID.value == "0") {
	    		$('#scenarioDetailsDiv').hide();
	    		alert("Please select Scenario Run ID");
	    		return false;
	    	} else {
	    	 	$('#scenarioDetailsDiv').show();
		 		$(".avgRespTime").css("background-color","#f7f7f7");
				graph = new SimpleGraph("chart1", {
		          "xmax": 500, "xmin": 0,
		          "ymax": 400, "ymin": 0, 
		          "title": "",
		          "xlabel": "Elapsed Time (sec)",
		          "ylabel": "Response Time (ms)",
		          "runId": vm.selLiveReportID.value
		        });  
			}
	    	return true;
		}
		
	    vm.showAvgRespTime = function(){
	    	$(".avgRespTime").css("background-color","#f7f7f7");
		}
	    
		vm.showHistoryReportsDetails = function(){
			
			$('ul.menuNav li a.performanceTab').addClass('active');
			$('ul.menuNav li a.historicreport').addClass('active');
			
			$('#chart1').html("");
	    	if (vm.selHistoryReportID.value == "0") {
	    		$('#scenarioDetailsDiv').hide();
	    		alert("Please select Scenario Run ID");
	    		return false;
	    	} else {
	    	 	$('#scenarioDetailsDiv').show();
		 		$(".avgRespTime").css("background-color","#f7f7f7");
				graph = new SimpleGraph("chart1", {
		          "xmax": 500, "xmin": 0,
		          "ymax": 400, "ymin": 0, 
		          "title": "",
		          "xlabel": "Elapsed Time (sec)",
		          "ylabel": "Response Time (ms)",
		          "runId": vm.selHistoryReportID.value
		        });  
			}
	    	return true;
		}
		
		vm.showDetails = false;
		
//		vm.onFileSelect = function(selFile){
//			 //$files: an array of files selected, each file has name, size, and type.
//			var sel = $("#fileObj").val();
//		    //for (var i = 0; i < sel.length; i++) {
//		      var $file = sel; //selFile[i];
//		      $fileUploader.Uploader({
//		        url: 'rest/performance/fileUpload',
//		        file: $file,
//		        progress: function(e){}
//		      }).
//		      then(function(data, status, headers, config) {
//		        // file is uploaded successfully
//		        console.log(data);
//		      }); 
//		    }
//		//}

		vm.testScenarioData;
		vm.tempfile;
		vm.uploadFile = function(){
	        var file = vm.myFile;
//	        console.log('file is ' + JSON.stringify(file));
	        var uploadUrl = "rest/performance/fileUpload";
	        fileUpload.uploadFileToUrl(file, uploadUrl)
	        			.then(getFileUploadSuccess,null);
	        
	        function getFileUploadSuccess(res){
	        	if(res) vm.showDetails = true;
	        	vm.testScenarioData = res.testScenarioDTOs;
	        	vm.tempfile = res.tempFileName;
	        	
//	        	console.log('promsie..........' + vm.testScenarioData[0].testName);
//	        	console.log('promsie..........' + vm.testScenarioData[1].testName);
//	        	console.log('promsie..........' + vm.testScenarioData[2].testName);
	        }
	    };
	    
	} // Controller end here......
	
	
//	angular
//		.module("synecenter")
//		.service('fileUpload', ['$http', function ($http) {
//			this.uploadFileToUrl = function(file, uploadUrl){
//	        var fd = new FormData();
//	        fd.append('file', file);
//	        $http.post(uploadUrl, fd, {
//	            transformRequest: angular.identity,
//	            headers: {'Content-Type': undefined}
//	        })
//	        .success(function(data, status, headers, config){
//	        	//vm.showDetails = true;
//	        	var info  = data.testScenarioDTOs
//	        	console.log('success!!!!' + info[0].testName);
//	        })
//	        .error(function(data, status, headers, config){
//	        	console.log('error!!!!1' + data);
//	        });
//	    }
//	}]);
	
	
	
	angular
		.module("synecenter")
		.directive('fileModel', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	            var model = $parse(attrs.fileModel);
	            var modelSetter = model.assign;
	            
	            element.bind('change', function(){
	                scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                });
	            });
	        }
	    };
	}]);
	
	
	
	
	
	angular.module("synecenter").directive("syToolDescription",function(){
			   return {
				   		restrict: "E",
				   		templateUrl: "app/performance/directives/ToolDescriptionDirectives.html"
			   		}
		   		});	
	
	angular.module("synecenter").directive("syCreateScenario",function(){
			   return {
				   restrict: "E",
				   templateUrl: "app/performance/directives/CreateScenarioDirectives.html"
			   }
		   });
	
	angular.module("synecenter").directive("syScheduleScenario",function(){
			   return {
				   restrict: "E",
				   templateUrl: "app/performance/directives/ScheduleScenarioDirectives.html"
			   }
		   });
	
	angular.module("synecenter").directive("syLiveReports",function(){
			   return {
				   restrict: "E",
				   templateUrl: "app/performance/directives/LiveReportsDirectives.html"
			   }
			});
	
	angular.module("synecenter").directive("syHistoricReports",function(){
			   return {
				   restrict: "E",
				   templateUrl: "app/performance/directives/HistoricReportsDirectives.html"
			   }
			});
	
}());