(function(){
	
	"use strick";
	
	angular.module("common.services")
			.factory("performanceConfigureService",
					["$resource",
					 performanceConfigureService]);
	
	function performanceConfigureService($resource){
		return $resource("rest/performance/configure");
	}
	
	
	
	angular.module("common.services")
	.factory("performanceConfigureService2",
			["$resource",
			 performanceConfigureService2]);
	
	function performanceConfigureService2($resource){
	
		 return $resource('rest/performance/removeLoadGenerator/:id', {}, {
		        post: { method: 'POST', params: {id: '@id'} }
		    })
		
	}
	
	
}());