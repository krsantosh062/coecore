(function(){

	//"use strict";
	
	var app = angular.module("performanceHomeMock",["ngMockE2E"]);
	
	app.run(function ($httpBackend){
		
		var recentTestData = [
								{
									"RunIDs":250,
									"ScenarioName":"Test Scenario 250",
									"DateTime":"2014-12-05",
									"TotalUser":6,
									"TotalTransactions":null,
									"Pass":12.0,
									"Fail":3.0,
									"HitsSec":null,
									"Status":"Completed"
								},
								{
									"RunIDs":251,
									"ScenarioName":"Test Scenario 251",
									"DateTime":"2014-12-08",
									"TotalUser":6,
									"TotalTransactions":4,
									"Pass":null,
									"Fail":null,
									"HitsSec":null,
									"Status":"Pending"
								},
								{
									"RunIDs":252,
									"ScenarioName":"Test Scenario 252",
									"DateTime":"2014-12-05",
									"TotalUser":200,
									"TotalTransactions":2,
									"Pass":null,
									"Fail":null,
									"HitsSec":null,
									"Status":"Pending"
								},
								{
									"RunIDs":253,
									"ScenarioName":"Test Scenario 253",
									"DateTime":"2014-12-05",
									"TotalUser":6,
									"TotalTransactions":3,
									"Pass":null,
									"Fail":null,
									"HitsSec":null,
									"Status":"Completed"
								},
								{
									"RunIDs":254,
									"ScenarioName":"Test Scenario 254",
									"DateTime":"2014-12-05",
									"TotalUser":200,
									"TotalTransactions":2,
									"Pass":null,
									"Fail":null,
									"HitsSec":null,
									"Status":"Pending"
								},
								{
									"RunIDs":255,
									"ScenarioName":"Test Scenario 255",
									"DateTime":"2014-12-05",
									"TotalUser":6,
									"TotalTransactions":3,
									"Pass":null,
									"Fail":null,
									"HitsSec":null,
									"Status":"Pending"
								},
								{
									"RunIDs":256,
									"ScenarioName":"Test Scenario 256",
									"DateTime":"2014-12-05",
									"TotalUser":6,
									"TotalTransactions":3,
									"Pass":null,
									"Fail":null,
									"HitsSec":null,
									"Status":"Pending"
								}
						];
		
		
			var url = "/api/recentTest";
			
			$httpBackend.whenGET(/\.html$/).passThrough();
			
			$httpBackend.whenGET(url).respond(recentTestData);
	});
	
}());