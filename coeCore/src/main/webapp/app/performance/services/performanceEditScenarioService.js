(function(){
	
	"use strick";
	
	angular
	.module("common.services")
	.service('fileUpload', ['$http', function ($http) {
		//this.retData= null;
		
		return {
			uploadFileToUrl:uploadFileToUrl
		};
		
		function uploadFileToUrl(file, uploadUrl){
		        
				var fd = new FormData();
		        fd.append('file', file);
		        
		       return  $http.post(uploadUrl, fd, {
		            transformRequest: angular.identity,
		            headers: {'Content-Type': undefined}
		        })
		        	.then(sendResponseData);
		        	//.catch(sendError);
		        
//		        .success(function(data, status, headers, config){
//		        	console.log('success this.retData !!!!' + this.retData[0].testName);
//		        })
//		        .error(function(data, status, headers, config){
//		        	console.log('error!!!!1' + data);
//		        });
		        
		}
		
		function sendResponseData(response){
			return response.data
		}
		function sendError(response){
			return $q.reject('Error uploading file!!!');
		}
		
	}]);
	
}());