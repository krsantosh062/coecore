(function(){
	
	"use strict";
	
	angular
		.module("common.services")
		.factory("performanceHomeServices",
				["$resource",
				 performanceHomeServices]);
	
	
	function performanceHomeServices($resource){
		 var data = $resource("rest/home/performance");
		  return data;
		
//		return $resource('rest/home/performance',{},{
//	        post:{
//	            method:"GET",
//	            isArray:false,
//	            headers:{'Access-Control-Allow-Origin':'*'} 
//	        },
//	    });
	}
	
}());