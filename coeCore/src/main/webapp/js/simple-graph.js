registerKeyboardHandler = function(callback) {
  var callback = callback;
  d3.select(window).on("keydown", callback);  
};

var tsFn = function(d, startTimestamp) { 
        var nextTimeSlot = new Date(parseInt(d));
		var startTime = new Date(startTimestamp);
		var timeDiff = nextTimeSlot - startTime;
		// strip the ms
		var timeDiff = timeDiff / 1000;
		// remove seconds from the date
		//timeDiff = Math.floor(timeDiff / 60);
		return Math.round(timeDiff % 60);	
		//x1 = Math.round(timeDiff % 60);
		//return Math.round(timeDiff % 60);		
}
var ltFn = function(d) { 
        return parseInt(d);
}

SimpleGraph = function(elemid, options) {
  var self = this;
  this.chart = document.getElementById(elemid);
  this.cx = this.chart.clientWidth;
  this.cy = this.chart.clientHeight;
  this.options = options || {};
  this.options.xmax = options.xmax || 30;
  this.options.xmin = options.xmin || 0;
  this.options.ymax = options.ymax || 10;
  this.options.ymin = options.ymin || 0;

  this.padding = {
     "top":    this.options.title  ? 40 : 20,
     "right":                 30,
     "bottom": this.options.xlabel ? 60 : 10,
     "left":   this.options.ylabel ? 70 : 45
  };

  this.size = {
    "width":  this.cx - this.padding.left - this.padding.right,
    "height": this.cy - this.padding.top  - this.padding.bottom
  };

  var jsonData = JSON.parse('{"sample":[' + $.ajax({
								url:"rest/jmeterService/lineGraphData/" + options.runId,
								async:false
					  }).responseText + ']}');
  
  this.jsonData = jsonData;
  var startTimestamp = parseInt(d3.min(d3.values(jsonData.sample)).ts);	
  
  var x1, y1;
 
  this.points = d3.range(Object.keys(jsonData.sample).length-1).map(function(i) {
					 if (jsonData && jsonData.sample[i] && jsonData.sample[i].ts && jsonData.sample[i].t)
					 {
						 if (startTimestamp ==  parseInt(jsonData.sample[i].ts)) {
					 		x1 = 0;
				  		 }
						 else {
							x1 = tsFn(jsonData.sample[i].ts, startTimestamp);
						 }

						 y1 = ltFn(jsonData.sample[i].t);
						 lbl = jsonData.sample[i].lb;
					 }
					 return { x: x1, y: y1, lbl: lbl }; 
				 }, self); 

  this.color = d3.scale.category10();   // set the colour scale	

	// Set the ranges
  this.x = d3.scale.linear().range([0, this.size.width]);
  this.y = d3.scale.linear().range([0, this.size.height]);	

  // x-scale
  /*this.x = d3.scale.linear()
		  .domain([d3.min(this.points, function(d) { return d.x; }) - 1, d3.max(this.points, function(d) { return d.x; }) + 1])
		  .range([0, this.size.width]);*/

   // drag x-axis logic
  this.downx = Math.NaN;

  // y-scale (inverted domain)
  /*this.y = d3.scale.linear()
      .domain([d3.max(this.points, function(d) { return d.y; }) + 1000, d3.min(this.points, function(d) { return d.y; }) - 1000])
      .nice()
      .range([0, this.size.height])
      .nice();*/

  // drag y-axis logic
  this.downy = Math.NaN;

  this.dragged = this.selected = null;

  var thiz = this;
  this.line = d3.svg.line()
      .x(function(d, i) { return this.x(d.x); })
      .y(function(d, i) { return this.y(d.y); });

  var xrange  = (this.options.xmax - this.options.xmin),
      yrange2 = (this.options.ymax - this.options.ymin) / 2,
      yrange4 = yrange2 / 2,
      datacount = this.size.width/30;

  this.vis = d3.select(this.chart).append("svg")
      .attr("width",  this.cx)
      .attr("height", this.cy)
      .append("g")
        .attr("transform", "translate(" + this.padding.left + "," + this.padding.top + ")");

  this.plot = this.vis.append("rect")
      .attr("width", this.size.width)
      .attr("height", this.size.height)
      .style("fill", "#EEEEEE")
      .attr("pointer-events", "all")
      .on("mousedown.drag", self.plot_drag())
      .on("touchstart.drag", self.plot_drag());
 this.plot.call(d3.behavior.zoom().x(this.x).y(this.y).on("zoom", this.redraw()));

 this.vis.append("svg")
      .attr("top", 0)
      .attr("left", 0)
      .attr("width", this.size.width)
      .attr("height", this.size.height)
      .attr("viewBox", "0 0 "+this.size.width+" "+this.size.height)
      .attr("class", "line");

  // add Chart Title
  if (this.options.title) {
    this.vis.append("text")
        .attr("class", "axis")
        .text(this.options.title)
        .attr("x", this.size.width/2)
        .attr("dy","-0.8em")
        .style("text-anchor","middle");
  }

  // Add the x-axis label
  if (this.options.xlabel) {
    this.vis.append("text")
        .attr("class", "axis")
        .text(this.options.xlabel)
        .attr("x", this.size.width/2)
        .attr("y", this.size.height)
        .attr("dy","2.4em")
        .style("text-anchor","middle");
  }

  // add y-axis label
  if (this.options.ylabel) {
    this.vis.append("g").append("text")
        .attr("class", "axis")
        .text(this.options.ylabel)
        .style("text-anchor","middle")
        .attr("transform","translate(" + -40 + " " + this.size.height/2+") rotate(-90)");
  }

  /////////////////// MULTI LINE GRAPH

    // Nest the entries by symbol
    var dataNest = d3.nest()
        .key(function(d) {return d.lb;})
        .entries(jsonData.sample);

    legendSpace = this.size.width/dataNest.length; // spacing for the legend
     
	// Scale the range of the data
	this.x.domain([d3.min(this.points, function(d) { return d.x; }) - 1, d3.max(this.points, function(d) { return d.x; }) + 1]);
	this.y.domain([d3.max(this.points, function(d) { return d.y; }) + 1000, d3.min(this.points, function(d) { return d.y; }) - 1000]);

    var margin = {top: 30, right: 20, bottom: 70, left: 50};
    // Loop through each symbol / key
    dataNest.forEach(function(d,i) { 

		this.range = d3.range(Object.keys(d.values).length-1).map(function(i) {
					 if (jsonData && d.values[i] && d.values[i].ts && d.values[i].t)
					 {
						 if (startTimestamp ==  parseInt(d.values[i].ts)) {
					 		x1 = 0;
				  		 }
						 else {
							x1 = tsFn(d.values[i].ts, startTimestamp);
						 }

						 y1 = ltFn(d.values[i].t);
						 lbl = d.values[i].lb;
					 }
					 return { x: x1, y: y1, lbl: lbl }; 
				 }, self); 
		
		thiz.vis.select("svg")
			  .append("path")
				  .attr("class", "line")
				  .style("stroke", function() { // Add the colours dynamically
					  return d.color = thiz.color(d.key); })	
				  .attr("id", 'tag'+d.key.replace(/[^a-zA-Z0-9]/g, '')) // assign ID
				  .attr("d", thiz.line(this.range));

        // Add the Legend
        d3.select('#chart1').append("text")
            .attr("x", (legendSpace/2)+i*legendSpace)  // space legend
            .attr("y", thiz.size.height + (margin.bottom/2)+ 5)
            .attr("class", "legend")    // style the legend
            .style("color", function() { // Add the colours dynamically
                return d.color = thiz.color(d.key); })
            .on("click", function(){
                // Determine if current line is visible 
                var active   = d.active ? false : true,
                newOpacity = active ? 0 : 1; 
                // Hide or show the elements based on the ID
                d3.select("#tag"+d.key.replace(/[^a-zA-Z0-9]/g, ''))
                    .transition().duration(100) 
                    .style("opacity", newOpacity); 
                // Update whether or not the elements are active
                d.active = active;
                })  
            .text(d.key); 

    });
	

  ///////////////////

  d3.select(this.chart)
      .on("mousemove.drag", self.mousemove())
      .on("touchmove.drag", self.mousemove())
      .on("mouseup.drag",   self.mouseup())
      .on("touchend.drag",  self.mouseup());

  this.redraw()();
};
  
//
// SimpleGraph methods
//

SimpleGraph.prototype.plot_drag = function() {
  var self = this;
  return function() {
    registerKeyboardHandler(self.keydown());
    d3.select('body').style("cursor", "move");
	d3.select('body').select('.tooltip').transition()		
				.duration(500)		
				.style("opacity", 0);	
    if (d3.event.altKey) {
      var p = d3.svg.mouse(self.vis.node());
      var newpoint = {};
      newpoint.x = self.x.invert(Math.max(0, Math.min(self.size.width,  p[0])));
      newpoint.y = self.y.invert(Math.max(0, Math.min(self.size.height, p[1])));
      self.points.push(newpoint);
      self.points.sort(function(a, b) {
        if (a.x < b.x) { return -1 };
        if (a.x > b.x) { return  1 };
        return 0
      });
      self.selected = newpoint;
      self.update();
      d3.event.preventDefault();
      d3.event.stopPropagation();
    }    
  }
};

SimpleGraph.prototype.update = function() {
  
  this.color = d3.scale.category10();   // set the color scale
  var self = this;

  var startTimestamp = parseInt(d3.min(d3.values(self.jsonData.sample)).ts);	
  var dataNest = d3.nest()
        .key(function(d) {return d.lb;})
        .entries(self.jsonData.sample);

  self.vis.select("svg").selectAll("circle").remove();
  var circle;
  dataNest.forEach(function(d,i) { 

		 this.range = d3.range(Object.keys(d.values).length-1).map(function(i) {
					 if (d.values[i] && d.values[i].ts && d.values[i].t)
					 {
						 if (startTimestamp ==  parseInt(d.values[i].ts)) {
					 		x1 = 0;
				  		 }
						 else {
							x1 = tsFn(d.values[i].ts, startTimestamp);
						 }

						 y1 = ltFn(d.values[i].t);
						 lbl = d.values[i].lb;
					 }
					 return { x: x1, y: y1, lbl: lbl }; 
				 }, self); 

	      var tag = this.range[0].lbl.replace(/[^a-zA-Z0-9]/g, '');
		  var lines = self.vis.select("svg").select('#tag'+tag)
					  .attr("d", self.line(this.range));
					

		  circle = self.vis.select("svg").selectAll("circle").attr("id", "cir"+tag)
					.data(this.range, function(v) { return v; });

		  // Define the div for the tooltip
		  var div = d3.select("body").append("div")	
			  .attr("class", "tooltip")				
			  .style("opacity", 0);

		  circle.enter().append("circle")
			  .attr("class", function(v) { return v === self.selected ? "selected" : null; })
			  .attr("cx",    function(v) { return self.x(v.x); })
			  .attr("cy",    function(v) { return self.y(v.y); })
			  .attr("r", 1.5)
			  .style("cursor", "auto")
			  .on("mouseover", function(v) {		
					div.transition()		
						.duration(200)		
						.style("opacity", .9);		
					div.html(v.lbl + "<br/>ET: " + v.x + ", RT: "  + v.y)	
						.style("left", (d3.event.pageX) + "px")		
						.style("top", (d3.event.pageY - 28) + "px");	
					})					
			   .on("mouseout", function(v) {		
					div.transition()		
						.duration(500)		
						.style("opacity", 0);	
			   });

		  circle
			  .attr("class", function(v) { return v === self.selected ? "selected" : null; })
			  .attr("cx",    function(v) { 
				return self.x(v.x); })
			  .attr("cy",    function(v) { return self.y(v.y); });
		
		if (d3.event && d3.event.keyCode) {
				d3.event.preventDefault();
				d3.event.stopPropagation();
		}

  });
  
  
  
}

/*SimpleGraph.prototype.datapoint_drag = function() {
  var self = this;
  return function(d) {
    registerKeyboardHandler(self.keydown());
    document.onselectstart = function() { return false; };
    self.selected = self.dragged = d;
    self.update();
    
  }
};*/

SimpleGraph.prototype.mousemove = function() {
  var self = this;
  return function() {
    var p = d3.svg.mouse(self.vis[0][0]),
        t = d3.event.changedTouches;
    
    if (self.dragged) {
      self.dragged.y = self.y.invert(Math.max(0, Math.min(self.size.height, p[1])));
      self.update();
    };
    if (!isNaN(self.downx)) {
      d3.select('body').style("cursor", "ew-resize");
      var rupx = self.x.invert(p[0]),
          xaxis1 = self.x.domain()[0],
          xaxis2 = self.x.domain()[1],
          xextent = xaxis2 - xaxis1;
      if (rupx != 0) {
        var changex, new_domain;
        changex = self.downx / rupx;
        new_domain = [xaxis1, xaxis1 + (xextent * changex)];
        self.x.domain(new_domain);
        self.redraw()();
      }
      d3.event.preventDefault();
      d3.event.stopPropagation();
    };
    if (!isNaN(self.downy)) {
      d3.select('body').style("cursor", "ns-resize");
      var rupy = self.y.invert(p[1]),
          yaxis1 = self.y.domain()[1],
          yaxis2 = self.y.domain()[0],
          yextent = yaxis2 - yaxis1;
      if (rupy != 0) {
        var changey, new_domain;
        changey = self.downy / rupy;
        new_domain = [yaxis1 + (yextent * changey), yaxis1];
        self.y.domain(new_domain);
        self.redraw()();
      }
      d3.event.preventDefault();
      d3.event.stopPropagation();
    }
  }
};

SimpleGraph.prototype.mouseup = function() {
  var self = this;
  return function() {
    document.onselectstart = function() { return true; };
    d3.select('body').style("cursor", "auto");
    d3.select('body').style("cursor", "auto");
    if (!isNaN(self.downx)) {
      self.redraw()();
      self.downx = Math.NaN;
      d3.event.preventDefault();
      d3.event.stopPropagation();
    };
    if (!isNaN(self.downy)) {
      self.redraw()();
      self.downy = Math.NaN;
      d3.event.preventDefault();
      d3.event.stopPropagation();
    }
    if (self.dragged) { 
      self.dragged = null 
    }
  }
}

SimpleGraph.prototype.keydown = function() {
  var self = this;
  return function() {
    if (!self.selected) return;
    switch (d3.event.keyCode) {
      case 8: // backspace
      case 46: { // delete
        var i = self.points.indexOf(self.selected);
        self.points.splice(i, 1);
        self.selected = self.points.length ? self.points[i > 0 ? i - 1 : 0] : null;
        self.update();
        break;
      }
    }
  }
};

SimpleGraph.prototype.redraw = function() {
  var self = this;
  return function() {
    var tx = function(d) { 
      return "translate(" + self.x(d) + ",0)"; 
    },
    ty = function(d) { 
      return "translate(0," + self.y(d) + ")";
    },
    stroke = function(d) { 
      return d ? "#ccc" : "#666"; 
    },
    fx = self.x.tickFormat(10),
    fy = self.y.tickFormat(10);

    // Regenerate x-ticks�
    var gx = self.vis.selectAll("g.x")
        .data(self.x.ticks(10), String)
        .attr("transform", tx);

    gx.select("text")
        .text(fx);

    var gxe = gx.enter().insert("g", "a")
        .attr("class", "x")
        .attr("transform", tx);

    gxe.append("line")
        .attr("stroke", stroke)
        .attr("y1", 0)
        .attr("y2", self.size.height);

    gxe.append("text")
        .attr("class", "axis")
        .attr("y", self.size.height)
        .attr("dy", "1em")
        .attr("text-anchor", "middle")
        .text(fx)
        .style("cursor", "ew-resize")
        .on("mouseover", function(d) { d3.select(this).style("font-weight", "bold");})
        .on("mouseout",  function(d) { d3.select(this).style("font-weight", "normal");})
        .on("mousedown.drag",  self.xaxis_drag())
        .on("touchstart.drag", self.xaxis_drag());

    gx.exit().remove();

    // Regenerate y-ticks�
    var gy = self.vis.selectAll("g.y")
        .data(self.y.ticks(10), String)
        .attr("transform", ty);

    gy.select("text")
        .text(fy);

    var gye = gy.enter().insert("g", "a")
        .attr("class", "y")
        .attr("transform", ty)
        .attr("background-fill", "#FFEEB6");

    gye.append("line")
        .attr("stroke", stroke)
        .attr("x1", 0)
        .attr("x2", self.size.width);

    gye.append("text")
        .attr("class", "axis")
        .attr("x", -3)
        .attr("dy", ".35em")
        .attr("text-anchor", "end")
        .text(fy)
        .style("cursor", "ns-resize")
        .on("mouseover", function(d) { d3.select(this).style("font-weight", "bold");})
        .on("mouseout",  function(d) { d3.select(this).style("font-weight", "normal");})
        .on("mousedown.drag",  self.yaxis_drag())
        .on("touchstart.drag", self.yaxis_drag());

    gy.exit().remove();
    self.plot.call(d3.behavior.zoom().x(self.x).y(self.y).on("zoom", self.redraw()));
    self.update();    
  }  
}

SimpleGraph.prototype.xaxis_drag = function() {
  var self = this;
  return function(d) {
    document.onselectstart = function() { return false; };
    var p = d3.svg.mouse(self.vis[0][0]);
    self.downx = self.x.invert(p[0]);
  }
};

SimpleGraph.prototype.yaxis_drag = function(d) {
  var self = this;
  return function(d) {
    document.onselectstart = function() { return false; };
    var p = d3.svg.mouse(self.vis[0][0]);
    self.downy = self.y.invert(p[1]);
  }
};