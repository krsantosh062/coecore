/**
 * 
 */
var showHide = 0;
var previousTestNumber = 0;
function navigateTab(url, targetId) {
	$.ajax({
		url : url,
		success : function(response) {
			// $('#compAndTypeListDiv').css('display', 'block');
			$('#' + targetId).html(response);
		}
	});
}

function readJMXInputFile() {
	var fileInputHidden = document.getElementById("fileInputHidden");
	fileInputHidden.click();
}

function validateFile() {
	var textInput = document.getElementById("browseFile");
	//alert(textInput.value);
}

function getFileName() {
	var fileInputHidden = document.getElementById("fileInputHidden");
	var textInput = document.getElementById("browseFile");
	textInput.value = fileInputHidden.value;
}

function submitUpload() {
	if (document.getElementById("browseFile").value == '') {
		alert('Please select a file');
	} else {
		document.getElementById("uploadForm").submit();
	}
}

function setPreviousTestData() {
	$("#testScenarioName_" + previousTestNumber + "_ViewSpan").text(
			$("#testScenarioName_" + previousTestNumber).val());
	$("#testScenarioThreadGroupNumber_" + previousTestNumber + "_ViewSpan")
			.text(
					$(
							"#testScenarioThreadGroupNumThreads_"
									+ previousTestNumber).val());
	$("#testScenarioPacing_" + previousTestNumber + "_ViewSpan").text(
			$("#testScenarioPacing_" + previousTestNumber).val());
	$("#testScenarioThinkTime_" + previousTestNumber + "_ViewSpan").text(
			$("#testScenarioThinkTime_" + previousTestNumber).val());
	$("#testScenarioStartUserPeriod_" + previousTestNumber + "_ViewSpan").text(
			$("#testScenarioStartUsersPeriod_" + previousTestNumber).val());
	$("#testScenarioStopUserPeriod_" + previousTestNumber + "_ViewSpan").text(
			$("#testScenarioStopUsersPeriod_" + previousTestNumber).val());

}

function setCurrentTestData(testNumber) {
	$("#testScenarioName_" + testNumber).val(
			$("#testScenarioName_" + testNumber + "_ViewSpan").text().trim());
	$("#testScenarioThreadGroupNumThreads_" + testNumber).val(
			$("#testScenarioThreadGroupNumber_" + testNumber + "_ViewSpan")
					.text().trim());
	$("#testScenarioPacing_" + testNumber).val(
			$("#testScenarioPacing_" + testNumber + "_ViewSpan").text().trim());
	$("#testScenarioThinkTime_" + testNumber).val(
			$("#testScenarioThinkTime_" + testNumber + "_ViewSpan").text()
					.trim());
	$("#testScenarioStartUsersPeriod_" + testNumber).val(
			$("#testScenarioStartUserPeriod_" + testNumber + "_ViewSpan")
					.text().trim());
	$("#testScenarioStopUsersPeriod_" + testNumber).val(
			$("#testScenarioStopUserPeriod_" + testNumber + "_ViewSpan").text()
					.trim());

}

function editTestScenario(testNumber) {
	if (previousTestNumber != 0) {
		setPreviousTestData();
	}
	$(".testScenarioEdit").css("display", "none");
	$(".testScenarioView").css("display", "block");
	$("#testScenarioName_" + testNumber + "_EditSpan").css("display", "block");
	$("#testScenarioThreadGroupNumber_" + testNumber + "_EditSpan").css(
			"display", "block");
	$("#testScenarioPacing_" + testNumber + "_EditSpan")
			.css("display", "block");
	$("#testScenarioThinkTime_" + testNumber + "_EditSpan").css("display",
			"block");
	$("#testScenarioStartUserPeriod_" + testNumber + "_EditSpan").css(
			"display", "block");
	$("#testScenarioStopUserPeriod_" + testNumber + "_EditSpan").css("display",
			"block");
	$("#testScenarioDuration_" + testNumber + "_EditSpan").css("display",
			"block");
	$("#testScenarioName_" + testNumber + "_EditSpan").css("display", "block");
	$("#testScenarioName_" + testNumber + "_ViewSpan").css("display", "none");
	$("#testScenarioNameView_" + testNumber + "_ViewSpan").css("display",
			"none");
	$("#testScenarioThreadGroupNumber_" + testNumber + "_ViewSpan").css(
			"display", "none");
	$("#testScenarioPacing_" + testNumber + "_ViewSpan").css("display", "none");
	$("#testScenarioThinkTime_" + testNumber + "_ViewSpan").css("display",
			"none");
	$("#testScenarioStartUserPeriod_" + testNumber + "_ViewSpan").css(
			"display", "none");
	$("#testScenarioStopUserPeriod_" + testNumber + "_ViewSpan").css("display",
			"none");
	setCurrentTestData(testNumber);
	$(".saveLink").css("display", "none");
	$("#testScenarioNumber_" + testNumber + "_SaveLink")
			.css("display", "block");
	previousTestNumber = testNumber;
}

function saveScenario(testNumber) {
	var testName = $("#testScenarioName_" + testNumber).val();
	var testThreadGroupNumThreads = $(
			"#testScenarioThreadGroupNumThreads_" + testNumber).val();
	var testPacing = $("#testScenarioPacing_" + testNumber).val();
	var testThinkTime = $("#testScenarioThinkTime_" + testNumber).val();
	var testStartUsersPeriod = $("#testScenarioStartUsersPeriod_" + testNumber)
			.val();
	var testStopUsersPeriod = $("#testScenarioStopUsersPeriod_" + testNumber)
			.val();
	var tempFileName = $("#testScenarioTempFileName").val();
	$.ajax({
		url : "saveScenario.html",
		data : {
			testNumber : testNumber,
			testName : testName,
			testThreadGroupNumThreads : testThreadGroupNumThreads,
			testPacing : testPacing,
			testThinkTime : testThinkTime,
			testStartUsersPeriod : testStartUsersPeriod,
			testStopUsersPeriod : testStopUsersPeriod,
			tempFileName : tempFileName
		},
		success : function(data) {
			$("#testScenarioDuration_" + testNumber + "_ViewSpan").html(
					data);
		}
	});
	$("#testScenarioName_" + testNumber + "_ViewSpan").text(
			$("#testScenarioName_" + testNumber).val());
	$("#testScenarioThreadGroupNumber_" + testNumber + "_ViewSpan").text(
			$("#testScenarioThreadGroupNumThreads_" + testNumber).val());
	$("#testScenarioPacing_" + testNumber + "_ViewSpan").text(
			$("#testScenarioPacing_" + testNumber).val());
	$("#testScenarioThinkTime_" + testNumber + "_ViewSpan").text(
			$("#testScenarioThinkTime_" + testNumber).val());
	$("#testScenarioStartUserPeriod_" + testNumber + "_ViewSpan").text(
			$("#testScenarioStartUsersPeriod_" + testNumber).val());
	$("#testScenarioStopUserPeriod_" + testNumber + "_ViewSpan").text(
			$("#testScenarioStopUsersPeriod_" + testNumber).val());
	$(".testScenarioEdit").css("display", "none");
	$(".testScenarioView").css("display", "block");
	$(".saveLink").css("display", "none");

}

function downloadJmx() {

	var tempFileName = $("#testScenarioTempFileName").val();
	tempFileName = document.getElementById("testScenarioTempFileName").value;
	$.ajax({
		url : "fileDownload.html",
		data : {
			tempFileName : tempFileName
		}
	});
}

function addLoadGenerator() {
	var loadGenName = $("#loadGenName").val();
	var loadGenIP = $("#loadGenIP").val();

	$.ajax({
		url : "addLoadGenerator",
		data : {
			loadGenName : loadGenName,
			loadGenIP : loadGenIP
		},
		success : function(response) {
			$("#loadGenDiv").html(response);
		}
	});
}

function addServer() {
	var serverName = $("#serverName").val();
	var serverTypeId = $("#serverTypeId").val();
	if (serverName.trim() == "" || serverTypeId == 0) {
		alert('Server name not entered or Server Type not selected.  Please try again');
	} else {
		$.ajax({
			url : "addServer",
			data : {
				serverName : serverName,
				serverTypeId : serverTypeId
			},
			success : function(response) {
				$("#serversDiv").html(response);
			}
		});
	}
}

function showLGsDiv() {
	$('#loadGenDiv').css("display", "block");
	$('#serversDiv').css("display", "none");
}

function showServersDiv() {
	$('#loadGenDiv').css("display", "none");
	$('#serversDiv').css("display", "block");
}

function showTab2() {
	// alert('Hello!');
	// $('.menu').hide();
	// $('.menu li a:first').show();
	// $('.menu li a:first').addClass('active');
	$('.menuNav li a').click(function() {
		$('.menuNav li a').removeClass('active');
		alert('This: ' + this.String);
		$(this).addClass('active');
		return false;
	});
}

function showTab1(divId) {
	$('#tabs .tabs-content').hide();
	$('#tabs div.tabs-content:first').show();
	$('#tabs ul li:first').addClass('active');
	$('#tabs ul li a').click(function() {
		$('#tabs ul li').removeClass('active');
		$('#tabs ul li span').removeClass();
		$(this).parent().addClass('active');
		$('#tabs .tabs-content').hide();
		$('#' + divId).show();
		if (divId != "overviewDiv") {
			$('#' + divId + 'Inner').addClass('default-skin');
			$('#' + divId + 'Inner').addClass('overview');
			setScrollBar(divId + 'Inner');
		}
		return false;
	});
}

function deleteLGs() {
	var lgList = $('input[name="LG"]:checked');
	var lgId = "";
	if (lgList.length > 0) {
		for ( var i = 0; i < lgList.length; i++) {
			lgId = lgList[i].value + ", " + lgId;
		}
		if (confirm('Are you sure you want to delete the selected Load Generators?')) {
			$.ajax({
				url : 'deleteLoadGenerators',
				data : {
					lgIdList : lgId
				},
				success : function(response) {
					$('#loadGeneratorListDiv').html(response);
				}
			});
		}

	} else {
		$('#loadGeneratorListMsgDiv').html(
				'Please select a Load Generator to delete.');
	}
}

function deleteServers() {
	var serverList = $('input[name="server"]:checked');
	var serverId = "";
	if (serverList.length > 0) {
		for ( var i = 0; i < serverList.length; i++) {
			serverId = serverList[i].value + ", " + serverId;
		}
		if (confirm('Are you sure you want to delete the selected Servers?')) {
			$.ajax({
				url : 'deleteServers',
				data : {
					serverIdList : serverId
				},
				success : function(response) {
					$('#serverListDiv').html(response);
				}
			});
		}

	} else {
		$('#serverListMsgDiv').html('Please select a Server to delete.');
	}
}

function beforeSubmit(arr, $form, options) {
	var fileDataIndex = -1;

	$.each(arr, function(index, value) {
		if (value.name == "fileName") {
			if (value.value.length == 0) {
				fileDataIndex = index;
			}
		}
	});

	if (fileDataIndex != -1) {
		arr.remove(fileDataIndex);
	}
}